# docker build -t apache:latest .
FROM centos:latest
MAINTAINER stoleas

RUN yum -y -q update
RUN yum -y -q install httpd
RUN yum clean all

RUN echo "" > /etc/httpd/conf.d/welcome.conf
RUN rm -rf /var/www/*

# docker run --expose 80 -p 80:80 -v /data/apache_data1:/var/www/html -ti apache:latest /bin/bash
# /usr/sbin/httpd